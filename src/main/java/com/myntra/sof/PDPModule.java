package com.myntra.sof;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.myntra.sof.pdpmodule.pdp.PdpImpl;
import com.myntra.sof.utils.EventListener;
import com.myntra.sof.utils.IEventListener;
import com.myntra.sof.utils.Response;

import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.ClientAuth;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.JksOptions;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;
//import io.netty.util.concurrent.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;

public class PDPModule<eventListener> extends BaseModule implements IEventListener {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(PDPModule.class.getName());
	
	private EventListener eventListener;
	private EventBus eb;
	private MqttClient client = null;
	
	private static final String COMMAND = "COMMAND";
	private Integer moduleStatus= BaseConstants.AgentConstants.STOPPED;

    public String moduleAddress = null;
    public String agentAddress = null;
    private String moduleName;
    private String moduleId;

	private HttpServer server;
	
	public String ModuleAddress = null;

	@Override
	protected void setupModule() {

		/*Called only once */

		setModuleID(getModuleID());
		setModuleName(getModuleName());

		VertxOptions options = new VertxOptions()
		.setEventBusOptions(new EventBusOptions()
			.setSsl(false)
			.setKeyStoreOptions(new JksOptions().setPath("keystore.jks").setPassword("wibble"))
			.setTrustStoreOptions(new JksOptions().setPath("keystore.jks").setPassword("wibble"))
			.setClientAuth(ClientAuth.REQUIRED)
		);
		
		Vertx.clusteredVertx(options, res -> {
			if (res.succeeded()) {
				eb = res.result().eventBus();

                moduleAddress = config().getJsonObject("moduleInfo").getString("moduleAddress");
                agentAddress = config().getJsonObject("ebAgentInfo").getString("agentAddress");

                logger.info("My Module Address is "+ moduleAddress);
                logger.info("My Agent Address is "+ agentAddress);

                EventListener.getInstance().registerAddress(moduleAddress);

                /* Add listeners to Events */
                EventListener.getInstance().addListener(moduleAddress, this);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                /*Posting Ready message to Agent */
                Response response = new Response();

                response.command = BaseConstants.AgentCommands.START;
                response.moduleAddress = moduleAddress;
                response.message = new JsonObject().put("state",BaseConstants.AgentConstants.READY);
                response.status = BaseConstants.SUCCESS;

                EventListener.getInstance().postMessage(agentAddress,response.jsonResponse(response));

            } else {
				logger.info("Failed: " + res.cause());
			}
		});
	}

    protected void enableCorsSupport(Router router) {
        Set<String> allowHeaders = new HashSet<>();
        allowHeaders.add("x-requested-with");
        allowHeaders.add("Access-Control-Allow-Origin");
        allowHeaders.add("origin");
        allowHeaders.add("Content-Type");
        allowHeaders.add("accept");
        // CORS support
        router.route().handler(CorsHandler.create("*")
                .allowedHeaders(allowHeaders)
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.DELETE)
                .allowedMethod(HttpMethod.PATCH)
                .allowedMethod(HttpMethod.PUT)
        );
    }

    private void dummyRequest(RoutingContext routingContext) {

        routingContext.request().response().end("Success");
    }


	public void startAppServer() {
		logger.info("Starting Server\n");
	
		Integer port = 8001; //config().getInteger("http.port", 8000);
		Router router = Router.router(vertx);

		BridgeOptions bridgeOptions = new BridgeOptions();

		bridgeOptions.setOutboundPermitted(Collections.singletonList(new PermittedOptions().setAddress(ModuleConstants.AppEbAddress)));
		bridgeOptions.setInboundPermitted(Collections.singletonList(new PermittedOptions().setAddress(ModuleConstants.AppEbAddress)));

        enableCorsSupport(router);

        router.route().handler(BodyHandler.create());
	
		router.route("/eventbus/*").handler(SockJSHandler.create(vertx).bridge(bridgeOptions));

        router.get("/storeassist").handler(this::dummyRequest);

        HttpServerOptions httpOpts = new HttpServerOptions();
        httpOpts.setPemKeyCertOptions(new PemKeyCertOptions()
                .setCertPath("server.crt")
                .setKeyPath("server.key"));
        httpOpts.setSsl(true);

        server = vertx.createHttpServer(httpOpts).requestHandler(router::accept).listen(port, "localhost");
        logger.info("Server Created Successfully\n");

		if(server != null)
		{
			logger.info("Server Created Successfully\n");
		} else {
			logger.info("Server Creation Failed");
		}
	}

	/* To be revisited */
	
	@Override
	protected void startModule(Message message) {

		if(moduleStatus == BaseConstants.AgentConstants.STOPPED || moduleStatus == BaseConstants.AgentConstants.READY){

		    Response response = new Response();

            MqttClientOptions mqttOptions = new MqttClientOptions();
            mqttOptions.setMaxMessageSize(268435456);
            client = MqttClient.create(Vertx.currentContext().owner(), mqttOptions);

            Future<Void> mqttFuture = connectMqttHost("104.211.241.205",1883);

            mqttFuture.setHandler(ar -> {
                if (ar.succeeded()) {
                    logger.info("MQTT CONNCTION SUCCEEDED");
                    //this.registerMqttTopic("sof.myntra.pdp.readstyles");
                    this.registerMqttTopic("1_1_PDP1.MAC_ID_MACHINE");
                } else {
                    logger.info("MQTT CONNECTION FAILED");
                }
            });

            startAppServer();

			PdpImpl.getInstance().openApp();

            EventListener.getInstance().postMessage(ModuleConstants.AppEbAddress,new JsonObject().put("command",ModuleConstants.PDPAppCommands.DISPLAY_LOADER));

            response.command = BaseConstants.AgentCommands.START;
            response.moduleAddress = moduleAddress;
            response.message = new JsonObject().put("state",BaseConstants.AgentConstants.STARTED);
            response.status = BaseConstants.SUCCESS;

            setModuleStatus(BaseConstants.AgentConstants.STARTED);

            message.reply(response.jsonResponse(response));

        }else{
            Response response = new Response();

			logger.info("Module is already Running, Start received");

            response.command = BaseConstants.AgentCommands.START;
            response.moduleAddress = moduleAddress;
            response.message = new JsonObject().put("state",moduleStatus);
            response.status = BaseConstants.SUCCESS;
            //
            message.reply(response.jsonResponse(response));
		}

		logger.info("Module Started");
	}

	/* To be revisited */

	@Override
	protected void stopModule(Message message) {

        if(moduleStatus == BaseConstants.AgentConstants.STARTED){

            PdpImpl.getInstance().closeApp();

            Response response = new Response();
            response.moduleAddress = moduleAddress;
            response.command = BaseConstants.AgentCommands.STOP;
            response.message = new JsonObject().put("state",BaseConstants.AgentConstants.STOPPED);
            response.status = BaseConstants.SUCCESS;

            setModuleStatus(BaseConstants.AgentConstants.STOPPED);

            logger.info("stopModule - Module Stopped");
            logger.info("stopModule - publishing response to Agent @ " + message.replyAddress());
            logger.info("Response is "+ response.toString());
            message.reply(response.jsonResponse(response));
            logger.info("Response sent to "+ response.toString());
        }else{

            Response response = new Response();
            response.moduleAddress = moduleAddress;
            response.command = BaseConstants.AgentCommands.STOP;
            response.message = new JsonObject().put("state",moduleStatus);
            response.status = BaseConstants.FAILURE;

            logger.info("stopModule - Module has stopped already - stop called again");
            logger.info("stopModule - publishing response to Agent @ " + message.replyAddress());
            message.reply(response.jsonResponse(response));
            logger.info("Response sent to "+ response.toString());
        }
	}

	private Future<Void> connectMqttHost(String host, Integer port){
		Future<Void> future = Future.future();
		client.connect(port, host, s -> {
			if(s.succeeded()){
				logger.info("Connected to Host - " + host + " on Port - "+port);
				// registerMqttTopic("sof.myntra.pdp.readstyles",client);
				future.complete();
			}
			else{
				logger.info("Failed to connect to Host - " + host + " on Port - "+port);
				future.fail(s.cause());
			}
		});

		return future;
	}

	private void disconnectMqttHost(){
		if(client.isConnected()){
			client.disconnect();
			logger.info("Clinet Disconnected");
		}
		else{
			logger.info("Clinet not connected to disconnect");
		}
	}

	private void registerMqttTopic(String topic){

		logger.info("Registering to Topic "+topic);
		
		client.publishHandler(s -> {

			logger.info("There are new messages in topic: " + s.topicName());
			logger.info("Content(as string) of the message: " + s.payload().toString());
			logger.info("QoS: " + s.qosLevel());
			//PdpImpl.getInstance().openPdp(s.payload().toString());
            JsonObject json = new JsonObject(s.payload().toString());

            logger.info("Opening App Now");
            PdpImpl.getInstance().openPdp(json);
		  }).exceptionHandler(s -> {
			logger.info("GOT AN EXCEPTION");
			logger.info(s.toString());
		})
			.subscribe(topic, 2);
	}

    private String getModuleID(){
        return moduleId;
    }

    private void setModuleID(String modID){
        moduleId = modID;
    }

    private String getModuleName()
    {
        return moduleName;
    }

    private void setModuleName(String modName)
    {
        moduleName = modName;
    }

    @Override
    protected void healthCheck(Message message){

        Response response = new Response();

	    response.moduleAddress = moduleAddress;
        response.command = BaseConstants.AgentCommands.HEALTH_CHECK;
        response.message = new JsonObject().put("state",moduleStatus);
        response.status = BaseConstants.SUCCESS;

    }

    public void getModuleDetails(Message message){
        Response response = new Response();

        response.moduleAddress = moduleAddress;
        response.command = BaseConstants.AgentCommands.GET_MODULE_DETAILS;
        response.message = new JsonObject().put("moduleName",getModuleName()).put("moduleId",getModuleID());
        response.status = BaseConstants.SUCCESS;

        message.reply(response.jsonResponse(response));
    }

    public void setModuleDetails(Message message){

        JsonObject json = (JsonObject) message.body();
        setModuleName(json.getString("ModuleName"));
        setModuleID(json.getString("ModuleID"));

        Response response = new Response();
        response.moduleAddress = moduleAddress;
        response.command = BaseConstants.AgentCommands.SET_MODULE_DETAILS;
        response.message = new JsonObject().put("state","SET");
        response.status = BaseConstants.SUCCESS;

        message.reply(response.jsonResponse(response));
    }

    /*To be refactored*/
    public void onEvent(Message message) {

        String address = message.address();
        JsonObject json = (JsonObject) message.body();

        Integer command = json.getInteger(COMMAND);

        logger.info("Message For "+address +"Command is "+command);

        switch(command){

            case BaseConstants.AgentCommands.START:
                startModule(message);
                break;

            case BaseConstants.AgentCommands.STOP:
                stopModule(message);
                break;

            case BaseConstants.AgentCommands.HEALTH_CHECK:
                healthCheck(message);
                break;

            case BaseConstants.AgentCommands.GET_MODULE_DETAILS:
                getModuleDetails(message);
                break;

            case BaseConstants.AgentCommands.SET_MODULE_DETAILS:
                setModuleDetails(message);
                break;

            default:
                logger.info("onEvent - No Handler Registered ");
                break;
        }
        logger.info("onEvent - Handling Done");
    }

    public void setModuleStatus(int value) {
        this.moduleStatus = value;
        logger.info("Set module status to "+value);
    }
}
