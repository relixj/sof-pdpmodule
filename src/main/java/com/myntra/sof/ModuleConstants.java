package com.myntra.sof;

public class ModuleConstants {

        public static final int FAILURE = 0;
        public static final int SUCCESS = 1;

        public static final String OLABI_PRODUCT_RESOLVE_API = "https://login.olabi.ooo/api/v1/rfid/product-enquiry";
        public static final String PDP_APP = "https://wwww.myntra.com";

        public static final String pdpAppURL = "https://myntwebstatic.blob.core.windows.net/sof/pdp-app";
        public static final String AppEbAddress = "com.myntra.sof.pdp-app";


    //Commands supported by RFID Module which can be called by other Modules
        public static class RFIDModuleCommands {

            public static final int ENCODE_TAGS = 0x1001;
            public static final int KILL_TAGS = 0x1002;
            public static final int DEACTIVATE_TAGS = 0x1003;
            public static final int CHECK_TAG_STATUS = 0x1004;
            public static final int TAG_LOCATION = 0x1005;
            public static final int RESTART = 0x1006;
        }

        public static class PDPAppCommands {

            public static final int DISPLAY_PDP = 0x2001;
            public static final int DISPLAY_LOADER = 0x2002;
            public static final int CLOSE_PDP = 0x2003;
            public static final int DISPLAY_VIDEO = 0x2004;
            public static final int DISPLAY_PROMO = 0x2005;
            public static final int DISPLAY_WELCOME_MSG = 0x2006;
            public static final int DISPLAY_OFFERS = 0x2007;
        }
}
