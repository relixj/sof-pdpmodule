package com.myntra.sof.pdpmodule.pdp;

import java.io.IOException;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import com.myntra.sof.ModuleConstants;
import com.myntra.sof.utils.EventListener;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

public final class PdpImpl{

    private static final Logger logger = (Logger) LoggerFactory.getLogger(PdpImpl.class.getName());
    private static WebClient client = WebClient.create(Vertx.currentContext().owner());

   // private static HttpClient httpClient = Vertx.currentContext().owner().createHttpClient();

    private final String mCommand = "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome --kiosk --allow-running-insecure-content --app=https://myntwebstatic.blob.core.windows.net/sof/pdp-app";

    private static PdpImpl pdpObj;
    private static Integer appStatus = 0;
    private Process proc = null;

	
	public static PdpImpl getInstance(){
		if(null ==  pdpObj){
			pdpObj = new PdpImpl();
		}
		return pdpObj;
	}

	private String[] getCommand() {
	    return new String[] {
	            "/Applications/Google", "\\Chrome.app/Contents/MacOS/Google",
               "\\Chrome", "--kiosk", "--allow-running-insecure-content" , "--app=https://myntwebstatic.blob.core.windows.net/sof/pdp-app"
        };
	}

	public void displayItems(JsonObject body) {

        logger.info("resolveTags - RFID_MAC_UUID -  " + body.getValue("RFID_MAC_UUID"));
        logger.info("resolveTags - TAG_READ_COUNT -  " + body.getValue("TAG_READ_COUNT"));
        logger.info("resolveTags - RFID_TAG_LIST -  " + body.getValue("RFID_TAG_LIST"));

        logger.info("resolveTags - Requesting - URL "+ModuleConstants.OLABI_PRODUCT_RESOLVE_API);

        HttpRequest<Buffer> req = client
        .getAbs(ModuleConstants.OLABI_PRODUCT_RESOLVE_API)
        .putHeader("Content-Type", "application/json; charset=utf-8")
        .setQueryParam("RFID_MAC_UUID", ""+body.getValue("RFID_MAC_UUID"))
        .setQueryParam("TAG_READ_COUNT", ""+body.getValue("TAG_READ_COUNT"))
        .setQueryParam("RFID_TAG_LIST", ""+body.getValue("RFID_TAG_LIST"));
        
        logger.info("URL "+req);

        req.send(ar -> {
            if (ar.succeeded()) {
                // Obtain response
                HttpResponse<Buffer> response = ar.result();
                JsonObject json = (JsonObject) response.bodyAsJsonObject();

                logger.info("resolveTags - Received response with status code " + response.statusCode());
                logger.info("resolveTags - Received response body " + response.body());

                /* Parse Response and pick up the first tag to display PDP */

                if (null != json ) {

                    JsonArray pdpItem = (json.getJsonObject("results", json)).getJsonArray("pdp_item_data");

                    if (null != pdpItem) {
                        Iterator<Object> keys = pdpItem.iterator();
                        while( keys.hasNext() ) {
                            JsonObject key = (JsonObject) keys.next();

                            if(key.containsKey("PRODUCT_CODE")){
                                openPdp(key.getString("PRODUCT_CODE"));
                                break; //Handling only one style at a time
                            }
                            else{
                                logger.info("Product Code is not present inside pdp_item_data");
                            }
                        }
                    }
                    else {
                        logger.info("Json Response is null");
                    }
                }
                else {
                    logger.info("NUll Response from Olabi");
                }

            } else {
                logger.info("resolveTags - Something went wrong " + ar.cause().getMessage());
            }
        });
    }

    public void openApp(){


        if(appStatus == 0) {
            Runtime rt = Runtime.getRuntime();

            try {
                logger.info("Opening App URL " + ModuleConstants.pdpAppURL);
                proc = rt.exec("open "+ModuleConstants.pdpAppURL);

                if(proc != null){
                    logger.info("App Opened");
                    appStatus = 1;
                }
            } catch (IOException e) {
                logger.info("Not able to open PDP App");
                e.printStackTrace();
            }
        } else {
            logger.info("App is already Running");
        }
    }

    private void closePdpOnTimeout(){
        Timer timer = new Timer();

        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                logger.info("Closing the pdp page on timeout");
                closeApp();
            }
        },5000);
    }

    public void openPdp(JsonObject data) {

        logger.info("Sending data now");

        openApp();

        EventListener.getInstance().postMessage(ModuleConstants.AppEbAddress,
                                                new JsonObject().put("command",ModuleConstants.PDPAppCommands.DISPLAY_PDP).put("data",data.toString()));

        closePdpOnTimeout();
    }

    public void closeApp() {

        EventListener.getInstance().postMessage(ModuleConstants.AppEbAddress,
                new JsonObject().put("command",ModuleConstants.PDPAppCommands.CLOSE_PDP));
        appStatus = 0;
    }
    
    public void openPdp(String styleId){

        Runtime rt = Runtime.getRuntime();
        String url = ModuleConstants.PDP_APP/* + styleId*/;
        //String[] commands = {"start \"\" d:\\Contnous Reader.exe"};
        logger.info("Opening App URL "+url);

        Process proc = null;
            try {
				proc = rt.exec("open "+url);
			} catch (IOException e) {
                logger.info("Not able to open PDP App");
				e.printStackTrace();
			}
    }
}